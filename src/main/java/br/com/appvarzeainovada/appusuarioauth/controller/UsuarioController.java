package br.com.appvarzeainovada.appusuarioauth.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import br.com.appvarzeainovada.appusuarioauth.dto.UsuarioDTO;
import br.com.appvarzeainovada.appusuarioauth.service.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService service;

	@GetMapping
	public List<UsuarioDTO> listar() {
		return service.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody UsuarioDTO cadastrar(@RequestBody @Valid UsuarioDTO usuario) {

		return service.create(usuario);
	}
}
