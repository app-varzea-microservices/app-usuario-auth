package br.com.appvarzeainovada.appusuarioauth.model;


import static util.PreCondition.notEmpty;
import static util.PreCondition.notNull;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
public final class Usuario {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) 
	private Integer id;
	
	private String email;
	
	private String login;
	
	private String senha;
	
	public Usuario(){}
	
	public Usuario(Builder builder){
		super();
		this.email = builder.email;
		this.senha = builder.senha;
		this.login = builder.login;
	}
	
	public void update(String email, String senha, String login){
		checkObject(email, senha, login);
		this.email = email;
		this.senha = senha;
		this.login = login;
	}
	
	public static Builder getBuilder() {
		return new Builder();
	}
	
	@NoArgsConstructor
	public static class Builder {
		
		private String email;
		
		public Builder email(String email){
			this.email = email;
			return this;
		}
		
		private String senha;
		public Builder senha(String senha){
			this.senha = senha;
			return this;
		}
		
		private String login;
		public Builder login(String login){
			this.login = login;
			return this;
		}
		
		public Usuario build(){
			Usuario build = new Usuario(this); 
			build.checkObject(build.getEmail(), build.getSenha(), build.getLogin());
			return build;
		}
	}
	
	private void checkObject(String email, String senha, String login) {
		notNull(login, "Login não pode ser nulo");
		notEmpty(login, "Login não pode estar vazio");
		notNull(email, "Email não pode ser nulo");
		notEmpty(email, "Email não pode estar vazio");
		notNull(senha, "Senha não pode ser nulo");
		notEmpty(senha, "Senha não pode estar vazio");
	}
}
