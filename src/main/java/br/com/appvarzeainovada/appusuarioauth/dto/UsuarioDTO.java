package br.com.appvarzeainovada.appusuarioauth.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.Comparator;

@Getter
@Setter
public class UsuarioDTO implements  Comparable<UsuarioDTO> {

    private Integer id;

    @NotEmpty
    private String login;

    @NotEmpty
    private String email;

    @NotEmpty
    private String senha;

    @Override
    public int compareTo(UsuarioDTO usuario) {

        if(usuario.getEmail() == null)
            return 0;
        return Comparator.comparing(UsuarioDTO::getEmail)
                .thenComparing(UsuarioDTO::getLogin)
                .compare(this, usuario);
    }
}

