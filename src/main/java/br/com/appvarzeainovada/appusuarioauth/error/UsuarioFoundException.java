package br.com.appvarzeainovada.appusuarioauth.error;

@SuppressWarnings("serial")
public class UsuarioFoundException extends RuntimeException{

	public UsuarioFoundException(String email){
		super(String.format("Email existente na base de dados: <%s>", email));
	}
}
