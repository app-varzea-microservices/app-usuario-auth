package br.com.appvarzeainovada.appusuarioauth.error;

@SuppressWarnings("serial")
public class UsuarioNotFoundException extends RuntimeException{

	public UsuarioNotFoundException(Integer id){
		super(String.format("Nenhum Usuario encontrado pelo id: <%s>", id));
	}
}
