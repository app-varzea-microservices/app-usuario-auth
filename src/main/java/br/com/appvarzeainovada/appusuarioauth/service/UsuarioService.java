package br.com.appvarzeainovada.appusuarioauth.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import br.com.appvarzeainovada.appusuarioauth.error.UsuarioFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.appvarzeainovada.appusuarioauth.dto.UsuarioDTO;
import br.com.appvarzeainovada.appusuarioauth.error.UsuarioNotFoundException;
import br.com.appvarzeainovada.appusuarioauth.model.Usuario;
import br.com.appvarzeainovada.appusuarioauth.repository.UsuarioRepository;
import br.com.appvarzeainovada.appusuarioauth.service.impl.UsuarioServiceImpl;

@Service
public final class UsuarioService implements UsuarioServiceImpl {

	@Autowired
	private UsuarioRepository repository;

	@Override
	public UsuarioDTO create(UsuarioDTO usuario) {

		UsuarioDTO found = findByLogin(usuario.getLogin());

		if(found.compareTo(usuario) > 0)
			throw new UsuarioFoundException(found.getEmail());

		Usuario persisted = Usuario.getBuilder()
				.email(usuario.getEmail())
				.login(usuario.getLogin())
				.senha(new BCryptPasswordEncoder().encode(usuario.getSenha()))
				.build();
		persisted = repository.save(persisted);
		
		return convertToDTO(persisted);
	}

	@Override
	public UsuarioDTO delete(Integer id) {
		Usuario deleted = findUsuarioById(id);
		repository.delete(deleted);
		return convertToDTO(deleted);
	}

	@Override
	public List<UsuarioDTO> findAll() {
		List<Usuario> UsuarioEntries = repository.findAll();
		return convertToDTOs(UsuarioEntries);
	}

	@Override
	public UsuarioDTO findById(Integer id) {
		Usuario found = findUsuarioById(id);
		return convertToDTO(found);
	}

	@Override
	public UsuarioDTO update(UsuarioDTO usuario) {
		Usuario updated = findUsuarioById(usuario.getId());
		updated.update(usuario.getEmail(), new BCryptPasswordEncoder().encode(usuario.getSenha()), usuario.getLogin());
		updated = repository.save(updated);
		
		return convertToDTO(updated);
	}
	
	private Usuario findUsuarioById(Integer id){
		Optional<Usuario> result = repository.findById(id);
		return result.orElseThrow(() -> new UsuarioNotFoundException(id));
	}
	
	public List<UsuarioDTO> convertToDTOs(List<Usuario> models){
		return models.stream()
				.map(this::convertToDTO)
				.collect(toList());
	}
	
	public UsuarioDTO convertToDTO (Usuario model){
		UsuarioDTO dto = new UsuarioDTO();
		
		dto.setId(model.getId());
		dto.setEmail(model.getEmail());
		dto.setLogin(model.getLogin());
		dto.setSenha(model.getSenha());
		
		return dto;
	}

	public UsuarioDTO findByLogin(String login) {
		Optional<Usuario> result = repository.findByLogin(login);
		return convertToDTO(result.orElse(new Usuario()));
	}
}
