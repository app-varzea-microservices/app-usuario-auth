package br.com.appvarzeainovada.appusuarioauth.service.impl;

import java.util.List;

import br.com.appvarzeainovada.appusuarioauth.dto.UsuarioDTO;


public interface UsuarioServiceImpl {

	UsuarioDTO create (UsuarioDTO usuario);
	
	UsuarioDTO delete (Integer id);
	
	List<UsuarioDTO> findAll();
	
	UsuarioDTO findById(Integer id);
	
	UsuarioDTO update (UsuarioDTO usuario);
}
